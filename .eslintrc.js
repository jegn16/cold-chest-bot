module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  'extends': [
    'google',
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module',
  },
  'plugins': [
    '@typescript-eslint',
  ],
  'rules': {
    'linebreak-style': [
      'error', (process.platform === 'win32' ? 'windows' : 'unix'),
    ],
    'max-len': [
      'error', {'code': 120},
    ],
  },
};
