import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from '../Core/Validator';

/** Validates if affected user exist
 * @class
 * @extends Validator
 */
export class AffectedExistsValidator extends Validator {
  /**
   * @inheritDoc
   */
  async validate(msg, args) : Promise<boolean> {
    const user = msg.mentions.users.first();

    return !!user;
  }

  /**
   * @inheritDoc
   */
  errorMessage(): string {
    return TranslationProvider.getTranslation('validation.target_missing');
  }
}
