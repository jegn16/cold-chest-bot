import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from '../Core/Validator';

/** Validates if user us admin
 * @class
 * @extends Validator
 */
export class IsAdminValidator extends Validator {
  /**
   * @inheritDoc
   */
  async validate(msg, args) : Promise<boolean> {
    const guildOwner = msg.guild.owner.user;

    return !!guildOwner.equals(msg.author);
  }

  /**
   * @inheritDoc
   */
  errorMessage(): string {
    return TranslationProvider.getTranslation('validation.not_admin');
  }
}
