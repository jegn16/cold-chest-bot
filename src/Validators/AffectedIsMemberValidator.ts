import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from '../Core/Validator';

/** Validates if affected user is a member of guild
 * @class
 * @extends Validator
 */
export class AffectedIsMemberValidator extends Validator {
  /**
   * @inheritDoc
   */
  async validate(msg, args) : Promise<boolean> {
    const user = msg.mentions.users.first();
    const member = msg.guild.member(user);

    return !!member;
  }

  /**
   * @inheritDoc
   */
  errorMessage(): string {
    return TranslationProvider.getTranslation('validation.target_not_member');
  }
}
