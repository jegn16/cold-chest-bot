import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from '../Core/Validator';

/** Validates if channel exist
 * @class
 * @extends Validator
 */
export class AffectedChannelExistsValidator extends Validator {
  /**
   * @inheritDoc
   */
  async validate(msg, args) : Promise<boolean> {
    const channel = msg.mentions.channels.first();

    return !!channel;
  }

  /**
   * @inheritDoc
   */
  errorMessage(): string {
    return TranslationProvider.getTranslation('validation.channel_missing');
  }
}
