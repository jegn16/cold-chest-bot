import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from '../Core/Validator';
const config = require('../../config_files/config.json');

/** Validates if argument provided are enough
 * @class
 * @extends Validator
 */
export class NumberArgsValidator extends Validator {
  /**
     * @inheritDoc
     */
  async validate(msg, args) : Promise<boolean> {
    return args.length >= this.getCommand().getNumberArguments();
  }

  /**
     * @inheritDoc
     */
  errorMessage(): string {
    const prefix = config.prefix;
    return TranslationProvider.getTranslation('validation.missing_arguments', {
      prefix: prefix,
      schema: this.getCommand().schema(),
    });
  }
}
