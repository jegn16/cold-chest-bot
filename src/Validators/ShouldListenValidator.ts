import {FileManagerProvider} from '../Providers/FileManagerProvider';
import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from '../Core/Validator';
import {FirebaseProvider} from '../Providers/FirebaseProvider';

/** Validates if user can trigger command
 * @class
 * @extends Validator
 */
export class ShouldListenValidator extends Validator {
    private fileManager = new FileManagerProvider();
    private firebaseManager = new FirebaseProvider();

    /**
     * @inheritDoc
     */
    async validate(msg, args) : Promise<boolean> {
      const guildId = msg.guild.id;
      const authorMember = msg.guild.member(msg.author);
      let isMuted = false;

      let guild = await this.firebaseManager.findGuild(guildId);
      if (!guild) {
        guild = {};
      }

      if (!guild.ignored_members) guild.ignored_members = {};

      const ignoredCoincidences = guild.ignored_members[authorMember.id.toString()];

      isMuted = ignoredCoincidences == undefined;

      return isMuted;
    }

    /**
     * @inheritDoc
     */
    errorMessage(): string {
      return TranslationProvider.getTranslation('validation.ignored');
    }
}
