import {Validator} from '../Core/Validator';
import {TranslationProvider} from '../Providers/TranslationProvider';

/** Validates if message was posted on a guild
 * @class
 * @extends Validator
 */
export class WasPostedOnGuildValidator extends Validator {
  /**
   * @inheritDoc
   */
  async validate(msg, args) : Promise<boolean> {
    return !!msg.guild;
  }

  /**
   * @inheritDoc
   */
  errorMessage(): string {
    return TranslationProvider.getTranslation('validation.not_guild');
  }
}
