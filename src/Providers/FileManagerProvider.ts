import {FileManager} from '../FileManagers/FileManager';

const config = require('../../config_files/config.json');

/** Class that providers file manipulation
 * @class
 * @implements {FileManager}
 */
export class FileManagerProvider implements FileManager {
    private readonly FileManagerClass = require('../FileManagers/' + config.file_manager + '.js');
    private readonly fileManager;

    /** Basic constructor
     * @hideconstructor
     */
    constructor() {
      this.fileManager = this.FileManagerClass.generateReader();
    }

    /**
     * @inheritDoc
     */
    deleteFile(path: string): Promise<boolean> {
      return this.fileManager.deleteFile(path);
    }

    /**
     * @inheritDoc
     */
    writeFile(path: string, content: string): Promise<boolean> {
      return this.fileManager.writeFile(path, content);
    }

    /**
     * @inheritDoc
     */
    readFile(path: string): Promise<string> {
      return this.fileManager.readFile(path);
    }

    /**
     * @inheritDoc
     */
    fileExist(path: string): Promise<boolean> {
      return this.fileManager.fileExist(path);
    }

    /**
     * @inheritDoc
     */
    createFile(path: string): Promise<boolean> {
      return this.fileManager.createFile(path);
    }

    /**
     * @inheritDoc
     */
    createFileSync(path: string): boolean {
      return this.fileManager.createFileSync(path);
    }

    /**
     * @inheritDoc
     */
    deleteFileSync(path: string): undefined {
      return this.fileManager.deleteFileSync(path);
    }

    /**
     * @inheritDoc
     */
    fileExistSync(path: string): boolean {
      return this.fileManager.fileExistSync(path);
    }

    /**
     * @inheritDoc
     */
    readFileSync(path: string): string {
      return this.fileManager.readFileSync(path);
    }

    /**
     * @inheritDoc
     */
    writeFileSync(path: string, content: string): undefined {
      return this.fileManager.writeFileSync(path, content);
    }

    /**
     * @inheritDoc
     */
    readDir(path: string): Promise<any> {
      return this.fileManager.readDir(path);
    }

    /**
     * @inheritDoc
     */
    readDirSync(path: string): any {
      return this.fileManager.readDirSync(path);
    }
}
