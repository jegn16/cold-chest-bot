import {Message} from 'discord.js';
import {CommandExecutor} from './CommandExecutor';
import {FirebaseProvider} from './FirebaseProvider';

/** Decorator of Command executor to support command aliases
 * @class
 * @implements {CommandExecutor}
 */
export class AliasCommandDecorator implements CommandExecutor {
    private commandExecutor: CommandExecutor;

    /** Default constructor
     * @param {CommandExecutor} commandExecutor Command Executor to be decorated
     */
    constructor(commandExecutor: CommandExecutor) {
      this.commandExecutor = commandExecutor;
    }

    /**
     * @inheritDoc
     */
    execute(command: string, msg: Message, args: any) {
      const firebaseProvider = new FirebaseProvider();
      return firebaseProvider.findGuild(msg.guild.id).then((guild) => {
        if (guild != null && guild.alias != null && guild.alias[command]) command = guild.alias[command];

        return this.commandExecutor.execute(command, msg, args);
      });
    }
}
