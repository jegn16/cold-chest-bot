import * as firebase from 'firebase';
import {AuthProvider} from './AuthProvider';

/** Class that provides a middleware with firebase
 * @class
 */
export class FirebaseProvider {
    private static firebase;
    private database;

    /** Default constructor
     */
    constructor() {
      this.database = firebase.database();
    }

    /** Configure connections with firebase
     * @param {AuthProvider} authProvider Configuration params
     * @static
     */
    static initializeFirebase(authProvider: AuthProvider) {
      firebase.initializeApp({
        apiKey: authProvider.firebaseApiKey(),
        projectId: authProvider.firebaseProjectId(),
        authDomain: authProvider.firebaseAuthDomain(),
        databaseURL: authProvider.firebaseDatabaseUrl(),
        storageBucket: authProvider.firebaseStorageBucket(),
        messagingSenderId: authProvider.firebaseSenderId(),
        applicationId: authProvider.firebaseAppId(),
      });
      this.auth(authProvider);
    }

    /** Authenticate authorized user to firebase
     * @param {AuthProvider} authProvider Configuration params
     * @private
     * @static
     */
    private static auth(authProvider: AuthProvider) {
      firebase.auth().signInWithEmailAndPassword(
          authProvider.firebaseEmail(),
          authProvider.firebasePassword(),
      ).catch((error) =>{
        console.log('Error login on firebase' + error);
      });
    }

    /** Retrieves guild information from firebase
     * @param {string} guildId Id of guild
     * @return {Promise<any>} Returns a promise with the data of guild
     */
    async findGuild(guildId: string): Promise<any> {
      const dataSnapshot = await this.database.ref('guilds/'+guildId).once('value');
      return dataSnapshot.toJSON();
    }

    /** Update information of guild
     * @param {string} guildId If of guild
     * @param {any} content Content to update guild with
     * @return {Promise<any>} Returns a promise with the data of guild updated
     */
    async updateGuild(guildId: string, content: any): Promise<any> {
      return await this.database.ref('guilds/'+guildId).update(content);
    }
}
