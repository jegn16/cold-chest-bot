import {Message} from 'discord.js';
import {CommandExecutor} from './CommandExecutor';
import {FirebaseProvider} from './FirebaseProvider';

/** Decorator that ats support to identifies gif personalized commands and execute them properly
 * @class
 * @implements {CommandExecutor}
 */
export class GifCommandExecutorDecorator implements CommandExecutor {
    private commandExecutor: CommandExecutor;

    /** Default constructor
     * @param {CommandExecutor} commandExecutor Command Executor to be decorated
     */
    constructor(commandExecutor: CommandExecutor) {
      this.commandExecutor = commandExecutor;
    }

    /**
     * @inheritDoc
     */
    execute(command: string, msg: Message, args: any) {
      const firebaseProvider = new FirebaseProvider();
      return firebaseProvider.findGuild(msg.guild.id).then((guild) => {
        if (guild != null && guild.gifs != null && guild.gifs[command]) {
          args = ['show', command].concat(args);
          command = 'Gif';
        }

        return this.commandExecutor.execute(command, msg, args);
      });
    }
}
