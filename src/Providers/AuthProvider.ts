const fs = require('fs');

/** Class to provider authentication information
 * @class
 */
export class AuthProvider {
    private auth = {
      token: '',
      firebase: {
        app_id: '',
        api_key: '',
        database_url: '',
        project_id: '',
        storage_bucket: '',
        messaging_sender_id: '',
        auth_domain: '',
        email: '',
        password: '',
      },
    };

    /** Default constructor
     * @param {string} path path of config file
     */
    constructor(path) {
      try {
        const authJson = fs.readFileSync(path);
        this.auth = JSON.parse(authJson);
      } catch (error) {
        console.log('auth.json file not found');
      }
    }

    /** Retrieves auth token
     * @return {string} value
     */
    authToken(): string {
      if (process.env.AUTH_TOKEN) {
        return process.env.AUTH_TOKEN;
      } else {
        return this.auth.token;
      }
    }

    /** Retrieves firebase api key
     * @return {string} value
     */
    firebaseApiKey(): string {
      if (process.env.FIREBASE_API_KEY) {
        return process.env.FIREBASE_API_KEY;
      } else {
        return this.auth.firebase.api_key;
      }
    }

    /** Retrieves firebase app id
     * @return {string} value
     */
    firebaseAppId(): string {
      if (process.env.FIREBASE_APP_ID) {
        return process.env.FIREBASE_APP_ID;
      } else {
        return this.auth.firebase.app_id;
      }
    }

    /** Retrieves firebase database url
     * @return {string} value
     */
    firebaseDatabaseUrl(): string {
      if (process.env.FIREBASE_DB_URL) {
        return process.env.FIREBASE_DB_URL;
      } else {
        return this.auth.firebase.database_url;
      }
    }

    /** Retrieves firebase project id
     * @return {string} value
     */
    firebaseProjectId(): string {
      if (process.env.FIREBASE_PROJECT_ID) {
        return process.env.FIREBASE_PROJECT_ID;
      } else {
        return this.auth.firebase.project_id;
      }
    }

    /** Retrieves firebase storage bucket
     * @return {string} value
     */
    firebaseStorageBucket(): string {
      if (process.env.FIREBASE_STORAGE_BUCKET) {
        return process.env.FIREBASE_STORAGE_BUCKET;
      } else {
        return this.auth.firebase.storage_bucket;
      }
    }

    /** Retrieves firebase sender id
     * @return {string} value
     */
    firebaseSenderId(): string {
      if (process.env.FIREBASE_SENDER_ID) {
        return process.env.FIREBASE_SENDER_ID;
      } else {
        return this.auth.firebase.messaging_sender_id;
      }
    }

    /** Retrieves firebase auth domain
     * @return {string} value
     */
    firebaseAuthDomain(): string {
      if (process.env.FIREBASE_AUTH_DOMAIN) {
        return process.env.FIREBASE_AUTH_DOMAIN;
      } else {
        return this.auth.firebase.auth_domain;
      }
    }

    /** Retrieves firebase auth email
     * @return {string} value
     */
    firebaseEmail(): string {
      if (process.env.FIREBASE_EMAIL) {
        return process.env.FIREBASE_EMAIL;
      } else {
        return this.auth.firebase.email;
      }
    }

    /** Retrieves firebase auth password
     * @return {string} value
     */
    firebasePassword(): string {
      if (process.env.FIREBASE_PASSWORD) {
        return process.env.FIREBASE_PASSWORD;
      } else {
        return this.auth.firebase.password;
      }
    }
}
