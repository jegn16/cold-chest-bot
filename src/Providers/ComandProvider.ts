/** Class that recover command class
 * @class
 */
export class CommandProvider {
  /** Recovers a command class
   * @param {string} command Command identifier
   * @return {object} Class descriptor
   */
  async get(command: string) {
    const commandDescriptor = require('../Commands/'+command+'Command.js');

    return commandDescriptor[command+'Command'];
  }
}
