import {FirebaseProvider} from './FirebaseProvider';
import {YoutubeProvider} from './YoutubeProvider';

/** Class that play music on a channel
 * @class
 */
export class MusicProvider {
    private channel;

    /** Default constructor
     * @param {object} channel Channel where music is being play in.
     */
    constructor(channel) {
      this.channel = channel;
    }

    /** Starts playing next song in queue
     */
    loadNextSong() {
      const firebaseProvider = new FirebaseProvider();
      const youtubeProvider = new YoutubeProvider();
      firebaseProvider.findGuild(this.channel.guild.id.toString()).then((guild) => {
        if (guild == null || guild.music_queue == undefined) return this.closeChannel();

        const musicQueue = [];
        Object.keys(guild.music_queue).map(function(key, index) {
          musicQueue.push(guild.music_queue[key]);
        });
        guild.music_queue = musicQueue;

        const nextSong = guild.music_queue.shift();
        console.log(`Youtube Link: ${nextSong}`);
        const songStream = youtubeProvider.getStreamFromUrl(nextSong);

        this.openChannel().then((connection) => {
          this.startMusic(connection, songStream);
          return firebaseProvider.updateGuild(this.channel.guild.id.toString(), guild);
        });
      });
    }

    /** Start playing a song
     * @param {object} connection Open connection to a channel
     * @param {object} stream Data stream to play on channel
     */
    startMusic(connection, stream) {
      const dispatch = connection.play(stream, {volume: 0.7, bitrate: 'auto'});
      dispatch.on('finish', (_) => {
        console.log(`Music finish execution`);
        this.loadNextSong();
        dispatch.destroy();
      });
      dispatch.on('error', (error) => {
        console.log(`Error playing music: ${error}`);
        dispatch.destroy();
        this.loadNextSong();
      });
    }

    /** Open communication with audio channel
     * @return {object} Connection to channel
     */
    async openChannel() {
      return this.channel.join();
    }

    /** Close communication with channel
     */
    closeChannel() {
      this.channel.leave();
    }
}
