import {Message} from 'discord.js';
import {CommandProvider} from './ComandProvider';
import {CommandExecutor} from './CommandExecutor';
import {Command} from '../Core/Command';

/** Simple implementation of command executioner
 * @class
 * @implements {CommandExecutor}
 */
export class SimpleCommandExecutor implements CommandExecutor {
  /**
   * @inheritDoc
   */
  execute(command: string, msg: Message, args: any) {
    const commandProvider = new CommandProvider();
    return commandProvider.get(command).then((CommandClass) => {
      const commandObject = new CommandClass(msg, args) as Command;
      return commandObject.validate().then((isValid) => {
        if (isValid) {
          return commandObject.execute();
        }
      });
    });
  }
}
