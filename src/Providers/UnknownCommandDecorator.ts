import {Message} from 'discord.js';
import {CommandExecutor} from './CommandExecutor';
import {FirebaseProvider} from './FirebaseProvider';
import {TranslationProvider} from './TranslationProvider';

/** Command executioner decorator to determine whether to send a reply when command is knot known or suppress it
 * @class
 * @implements {CommandExecutor}
 */
export class UnknownCommandDecorator implements CommandExecutor {
    private commandExecutor: CommandExecutor;

    /** Default constructor
     * @param {CommandExecutor} commandExecutor Command Executor to be decorated
     */
    constructor(commandExecutor: CommandExecutor) {
      this.commandExecutor = commandExecutor;
    }

    /**
     * @inheritDoc
     */
    execute(command: string, msg: Message, args: any) {
      const firebaseProvider = new FirebaseProvider();
      let suppress = false;
      return firebaseProvider.findGuild(msg.guild.id).then((guild) => {
        if (guild != null && guild.configs != null &&
          guild.configs['suppress_unknown'] != null &&
          guild.configs['suppress_unknown'] == 'true') {
          suppress = true;
        }

        return this.commandExecutor.execute(command, msg, args).catch( (error) => {
          console.error('Error was encountered: ' + error.stack);
          if (!suppress) {
            msg.reply(TranslationProvider.getTranslation('general.unknown_command'));
          }
        });
      });
    }
}
