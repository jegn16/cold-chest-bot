import {FileManagerProvider} from './FileManagerProvider';
import jsyaml = require('js-yaml');

/** Class to provider internationalization of text provider to users
 * @class
 */
export class TranslationProvider {
    static DEFAULT_LANGUAGE = 'en';
    static LANGUAGE = null;
    static TRANSLATIONS: object = {};

    /** Load translations for all available languages and set default language to use
     * @static
     * @method
     */
    static initialize() {
      this.LANGUAGE = this.DEFAULT_LANGUAGE;
      const fileManager = new FileManagerProvider();
      const languageFiles = fileManager.readDirSync('./locales').filter((file) => file.endsWith('.yml'));

      for (const languageFile in languageFiles) {
        if (!Object.prototype.hasOwnProperty.call(languageFiles, languageFile)) continue;
        const languageFilename = languageFiles[languageFile];

        const translations = jsyaml.load(fileManager.readFileSync('./locales/' + languageFilename));

        this.TRANSLATIONS = Object.assign(this.TRANSLATIONS, translations);
      }
    }

    /** Set language to which text is internationalize to
     * @param {string} locale Language to set as current language to provider
     */
    static setLanguage(locale: string) {
      this.LANGUAGE = locale;
    }

    /** Check if a language as translations available
     * @param {string} locale Language to confirm if it is supported by translations
     * @return {boolean} Status whether language is supported or not
     */
    static isLanguageSupported(locale: string): boolean {
      let isSupported = false;
      Object.keys(this.TRANSLATIONS).map(function(key, index) {
        if (key.toLowerCase().localeCompare(locale) === 0) {
          isSupported = true;
        }
      });
      return isSupported;
    }

    /** Retrieves translation of a text by is identifier and substitude placeholder with variables if present
     * @param {string} identifier Identifier of text to translate
     * @param {object} variables Pair ob key:value to replace placeholders with the provided value
     * @return {string} Internationalize string
     */
    static getTranslation(identifier: string, variables = {}): string {
      const steps = identifier.split('.');

      let translation = this.TRANSLATIONS[this.LANGUAGE];
      try {
        for (const stepIndex in steps) {
          if (!Object.prototype.hasOwnProperty.call(steps, stepIndex)) continue;
          const step = steps[stepIndex];

          translation = translation[step];
        }

        if (!!variables) {
          Object.keys(variables).map(function(key, index) {
            translation = translation.replace(`%${key}%`, variables[key]);
          });
        }

        return translation;
      } catch {
        // eslint-disable-next-line no-throw-literal
        throw 'No translation was found';
      }
    }
}
