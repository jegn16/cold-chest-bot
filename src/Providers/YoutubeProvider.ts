const {URL, parse} = require('url');

/** Class that provides youtube functionalities
 * @class
 */
export class YoutubeProvider {
    private ytdl = require('ytdl-core');
    private ytsr = require('ytsr');

    /** Retrieves the first result of a search
     * @param {string} searchString Criteria to make the search
     * @return {Promise<string>} Returns a promise with url of first result
     */
    getFirstLink(searchString: string): Promise<string> {
      return this.searchByCriteria(searchString, 1);
    }

    /** Retrieves stream data from a url
     * @param {string} url
     * @return {any} Returns stream data
     */
    getStreamFromUrl(url: string) {
      return this.ytdl(url, {quality: 'highestaudio', filter: 'audioonly'});
    }

    /** Verifies that a string is a url from youtube
     * @param {string} urlString
     * @return {boolean} Returns if string is a url from youtube
     */
    validateUrl(urlString): boolean {
      try {
        new URL(urlString);
        const parsed = parse(urlString);
        return ['https:', 'http:'].includes(parsed.protocol) && parsed.host.startsWith('www.youtube');
      } catch (err) {
        return false;
      }
    }

    /** Retrieves metadata from a url
     * @param {string} url
     * @return {any} Returns metadata
     */
    async getMetadataFromUrl(url: string) {
      const metadata = await this.ytdl.getInfo(url);
      return metadata.videoDetails;
    }

    /** Makes a search of youtube links by a criteria
     * @param {string} searchString Criteria to make search by
     * @param {number} limit Number of elementes that should return as result
     * @private
     * @return {Promise<string>} Returns a promise with the list of search
     */
    private searchByCriteria(searchString: string, limit: number): Promise<string> {
      return this.ytsr.getFilters(searchString).then((filters) => {
        const filter = filters.get('Type').find( (o) => o.name = 'Video');
        const options = {
          limit: 1,
          nextpageRef: filter.ref,
        };
        return this.ytsr(filter.query, options);
      }).then((searchResults) => {
        const url = searchResults.items[0].link;
        console.log(`Youtube Link: ${url}`);
        return url;
      });
    }
}
