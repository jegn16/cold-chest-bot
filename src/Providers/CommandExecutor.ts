import {Message} from 'discord.js';

/** Interface of command executioner
 *  @interface
 */
export interface CommandExecutor {

    /** Executes command
     * @param {string} command Key Identifier of command to be executed
     * @param {Message} msg Message that requested a command to be executed
     * @param {any} args Arguments provided to command
     * @return {Promise<any>}
     */
    execute(command: string, msg: Message, args: any): Promise<any>;

}
