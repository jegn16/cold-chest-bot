import {Message, TextChannel} from 'discord.js';

/** Task that Copy a Message to another channel
 * @param {Message} msg Message to copy
 * @param {String} channelID Id of channel where the copy should be send
 * @method
 */
export function CopyMessageTask(msg: Message, channelID: string) {
  if (msg.channel.type !== 'dm') {
    const newChannel = msg.channel.guild.channels.cache.filter((value) => {
      return value.id == channelID;
    }).first() as TextChannel;

    const files = [];
    msg.attachments.map((value) => {
      return value.url;
    });

    newChannel.send(msg.content, {
      files: files,
    });
  }
}
