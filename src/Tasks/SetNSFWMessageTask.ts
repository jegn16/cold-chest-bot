import {Message} from 'discord.js';

/** Suppress a embed so id cannot be normally be seen
 * @param {Message} msg Message with embed to suppress
 * @param {String} channelID
 * @method
 */
export function SetNSFWMessageTask(msg: Message, channelID: string) {
  msg.suppressEmbeds(true);
}
