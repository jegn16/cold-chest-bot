import {Message, TextChannel} from 'discord.js';
import {TranslationProvider} from '../Providers/TranslationProvider';

/** Task to make a reference to a message in another channel
 * @param {Message} msg Message to reference
 * @param {string} channelID  Channel where reference should be send
 * @method
 */
export function ReferenceMessageTask(msg: Message, channelID: string) {
  if (msg.channel.type !== 'dm') {
    const newChannel = msg.channel.guild.channels.cache.filter((value) => {
      return value.id == channelID;
    }).first() as TextChannel;

    newChannel.send(TranslationProvider.getTranslation('tasks.reference.reference', {url: msg.url}));
  }
}
