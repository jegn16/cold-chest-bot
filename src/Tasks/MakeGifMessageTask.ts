import {DMChannel, Message} from 'discord.js';
import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {TranslationProvider} from '../Providers/TranslationProvider';

const config = require('../../config_files/config.json');

/** Task that converts a message to a gif
 * @param {Message} msg Message to convert to gif
 * @method
 */
export function MakeGifMessageTask(msg: Message) {
  msg.author.send(TranslationProvider.getTranslation('tasks.make_gif.request', {url: msg.url})).then((requestMsg) => {
    const channel = requestMsg.channel as DMChannel;

    return channel.awaitMessages((message) => message.content != '', {max: 1, time: 600000, errors: ['time']});
  }).then((messages) => {
    const replyMsg = messages.first();

    const name = replyMsg.content;

    const firebaseManager = new FirebaseProvider();
    const guildId = msg.guild.id;
    return firebaseManager.findGuild(guildId).then((guild) => {
      if (!guild) guild = {};
      if (!guild.gifs)guild.gifs = {};

      if (guild.gifs[name]) {
        replyMsg.reply(TranslationProvider.getTranslation('tasks.make_gif.already_used'));
        // eslint-disable-next-line no-throw-literal
        throw 'Already in Use';
      }

      let content = '';
      if (msg.attachments.size == 0) {
        content = msg.content;
      } else {
        content = msg.attachments.first().url;
      }

      guild.gifs[name] = [content];

      return firebaseManager.updateGuild(guildId, guild).then( (_) =>{
        replyMsg.reply(TranslationProvider.getTranslation('tasks.make_gif.registered'));
        return name;
      });
    });
  }).then((name) => {
    msg.reply(TranslationProvider.getTranslation('tasks.make_gif.notification', {command: config.prefix +name}));
  }).catch((error) =>{
    msg.reply(TranslationProvider.getTranslation('tasks.make_gif.error'));
  });
}
