import {FileManagerProvider} from '../Providers/FileManagerProvider';

const constants = require('../../config_files/constants.json');

/** Task to instantiate default guild config
 * @class
 */
export class ConfigGuildTask {
    private fileManager = new FileManagerProvider();

    /** Creates default guild settings
     * @param {Guild} guild Guild to create a profile to
     */
    createSettingsGuild(guild) {
      const guildId = +guild.id;
      const guildFile = constants.gilds_folder+guildId+'.json';
      const systemChannel = guild.systemChannel;

      this.fileManager.writeFile(guildFile, JSON.stringify({
        id: guildId,
        channels: [systemChannel.id],
        ignored_members: [],
      })).then((_) => {
        systemChannel.send(`I'll humor you with an introduction. \n\nI am ${[guild.client.user]}. 
        \nFrom here onwards I shall do as I please here in ${[guild]}`);
      }).catch((error) => {
        systemChannel.send(`Something went wrong while summoning me here. I may not be able to use all my powers`);
      });
    }
}
