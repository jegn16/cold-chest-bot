import Discord = require('discord.js');
import {AuthProvider} from './Providers/AuthProvider';
import {FirebaseProvider} from './Providers/FirebaseProvider';
import {SimpleCommandExecutor} from './Providers/SimpleCommandExecutor';
import {AliasCommandDecorator} from './Providers/AliasCommandDecorator';
import {GifCommandExecutorDecorator} from './Providers/GifCommandExecutorDecorator';
import {TranslationProvider} from './Providers/TranslationProvider';
import {LanguageExecutorDecorator} from './Providers/LanguageExecutorDecorator';
import {TextChannel} from 'discord.js';
import {UnknownCommandDecorator} from './Providers/UnknownCommandDecorator';


const config = require('../config_files/config.json');
const client = new Discord.Client();
const authProvider = new AuthProvider('./config_files/auth.json');

client.on('ready', () => {
  console.log('Logged in as '+ client.user.tag);
});

/* client.on('guildCreate',
        guild => new ConfigGuildTask().createSettingsGuild(guild)); */

client.on('message', (msg) => {
  if (msg.guild)console.log('Received message from: ' + msg.guild.id);
  if (msg.channel)console.log('Received in channel ' + msg.channel.id);
  TranslationProvider.setLanguage('en');
  console.log('Received by user ' + msg.author.username);

  const commandBreaker = ' '+ config.command_breaker +' ';
  const commands = msg.content.split(commandBreaker);

  for (let i = 0; i < commands.length; i++) {
    const encryptedCommand = commands[i];

    if (!encryptedCommand.startsWith(config.prefix)) return;

    const args = encryptedCommand.slice(config.prefix.length).split(/ +/);
    const command = args.shift();

    let commandExecutor = new SimpleCommandExecutor();
    if (msg.guild) {
      commandExecutor = new UnknownCommandDecorator(commandExecutor);
      commandExecutor = new LanguageExecutorDecorator(commandExecutor);
      commandExecutor = new AliasCommandDecorator(commandExecutor);
    }
    commandExecutor = new GifCommandExecutorDecorator(commandExecutor);
    commandExecutor.execute(command, msg, args);
  }
});

client.on('messageReactionAdd', async (msgReact) => {
  console.log('Reaction detected');

  const firebaseProvider = new FirebaseProvider();
  const guild = await firebaseProvider.findGuild(msgReact.message.guild.id);
  if (!guild.tasks)guild.tasks = {};
  const tasksOfEvent = guild.tasks['reaction'];
  const tasks = [];
  Object.keys(tasksOfEvent).map(function(key, index) {
    tasks.push(tasksOfEvent[key]);
  });
  tasks.forEach((tasks) => {
    const emoji = msgReact.emoji;
    const message = msgReact.message;
    const totalReactions = msgReact.message.reactions.cache.size;

    const sameEmojiCount = message.reactions.cache.filter((value) => {
      return value.emoji.name == tasks.expression;
    }).size;

    if (sameEmojiCount < tasks.total) {
      console.log('Requirements not meet yet');
      return null;
    }

    const action = tasks.action;

    const taskDescriptor = require('./Tasks/'+action+'MessageTask.js');
    const tasksMethod = taskDescriptor[action+'MessageTask'];

    if (tasks.target) {
      tasksMethod(msgReact.message, tasks.target);
    } else {
      tasksMethod(msgReact.message);
    }

    console.log('Emoji: ' + emoji.name);
    console.log('Total of emoji: ' + sameEmojiCount);
    console.log('Total: ' + totalReactions);
  });
});

// Snippet adapted from: https://github.com/AnIdiotsGuide/discordjs-bot-guide/blob/master/coding-guides/raw-events.md
// @ts-ignore
client.on('raw', async (packet) => {
  // We don't want this to run on unrelated packets
  if (!['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) return;
  // Grab the channel to check the message from
  const channel = await client.channels.fetch(packet.d.channel_id);
  // There's no need to emit if the message is cached, because the event will fire anyway for that
  if (channel.type != 'text') return;
  const textChannel = channel as TextChannel;
  if (textChannel.messages.cache.filter((msg) => msg.id == packet.d.message_id).size != 0) return;
  // Since we have confirmed the message is not cached, let's fetch it
  const message = await textChannel.messages.fetch(packet.d.message_id);
  // Emojis can have identifiers of name:id format, so we have to account for that case as well
  const emoji = packet.d.emoji.id ? `${packet.d.emoji.name}:${packet.d.emoji.id}` : packet.d.emoji.name;
  // This gives us the reaction we need to emit the event properly, in top of the message object
  const reaction = message.reactions.resolve(emoji);
  // Adds the currently reacting user to the reaction's users collection.
  if (reaction) await reaction.users.fetch(packet.d.user_id);
  // Check which type of event it is before emitting
  if (packet.t === 'MESSAGE_REACTION_ADD') {
    client.emit('messageReactionAdd', reaction, reaction.users.resolve(packet.d.user_id));
  }
  if (packet.t === 'MESSAGE_REACTION_REMOVE') {
    client.emit('messageReactionRemove', reaction, client.users.resolve(packet.d.user_id));
  }
});

client.login(authProvider.authToken());
FirebaseProvider.initializeFirebase(authProvider);
TranslationProvider.initialize();
