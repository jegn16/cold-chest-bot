/** Interface to interact with files
 * @interface
 */
export interface FileManager{
    /** Read content of a file
     * @param {string} path Location of file
     * @return {Promise<string>} Returns a promise with content of file
     */
    readFile(path: string): Promise<string>

    /** Write text on file
     * @param {string} path Location of file
     * @param {string} content Text to store on file
     * @return {Promise<boolean>} Returns a promise with a status of success or failure
     */
    writeFile(path: string, content: string): Promise<boolean>

    /** Create a file
     * @param {string} path Location of file
     * @return {Promise<boolean>} Returns a promise with a status of success or failure
     */
    createFile(path: string): Promise<boolean>

    /** Checks if file exist
     * @param {string} path Location of file
     * @return {Promise<boolean>} Returns a promise with a status of success or failure
     */
    fileExist?(path: string): Promise<boolean>

    /** Deletes a file
     * @param {string} path Location of file
     * @return {Promise<boolean>} Returns a promise with a status of success or failure
     */
    deleteFile(path: string): Promise<boolean>

    /** Read content of a file synchronous
     * @param {string} path Location of file
     * @return {string} Returns content of file
     */
    readFileSync(path: string): string

    /** Write text on file synchronous
     * @param {string} path Location of file
     * @param {string} content Text to store on file
     * @return {undefined} Returns undefined
     */
    writeFileSync(path: string, content: string): undefined

    /** Create a file synchronous
     * @param {string} path Location of file
     * @return {boolean} Returns status of success or failure
     */
    createFileSync(path: string): boolean

    /** Checks if file exist synchronous
     * @param {string} path Location of file
     * @return {boolean} Returns status of success or failure
     */
    fileExistSync?(path: string): boolean

    /** Deletes a file synchronous
     * @param {string} path Location of file
     * @return {undefined} Returns undefined
     */
    deleteFileSync(path: string): undefined

    /** Return a list of all files in a folder
     * @param {string} path Location of folder
     * @return {Promise<any>} Return a promise with a list of files
     */
    readDir(path: string): Promise<any>

    /** Return a list of all files in a folder synchronous
     * @param {string} path Location of folder
     * @return {any} return a list of files
     */
    readDirSync(path: string): any
}
