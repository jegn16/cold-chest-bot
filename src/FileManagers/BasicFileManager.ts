import {FileManager} from './FileManager';

const fs = require('fs');

/** Returns a file Managers
 * @method
 * @return {FileManager} returns a File Manager
 */
export function generateReader(): FileManager {
  return new BasicFileManager();
}

/** Basic implementation of a File Manager
 * @class
 * @implements {FileManager}
 */
export class BasicFileManager implements FileManager {
  /**
   * @inheritDoc
   */
  createFile(path: string): Promise<boolean> {
    /* Currently unnecessary sin fs.writeFile will create file if id does not exists */
    // eslint-disable-next-line no-throw-literal
    throw 'Not implemented';
  }

  /**
   * @inheritDoc
   */
  fileExist(path: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      fs.stat(path, (error, stats) => {
        if (error) {
          console.error('File could not be found. \n File Path: ' + path + '\n Error: ' + error);
          return resolve(false);
        }

        resolve(stats.isFile());
      });
    });
  }

  /**
   * @inheritDoc
   */
  readFile(path: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      fs.readFile(path, function(error, data) {
        if (error) {
          console.error('File could not be read. \n File Path: ' + path + '\n Error: ' + error);
          // eslint-disable-next-line prefer-promise-reject-errors
          return reject('Filed could not be read.');
        }

        resolve(data);
      });
    });
  }

  /**
   * @inheritDoc
   */
  writeFile(path: string, content: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      fs.writeFile(path, content, (error) => {
        if (error) {
          console.error('File could not be written. \n File Path: ' + path + '\n Error: ' + error);
          // eslint-disable-next-line prefer-promise-reject-errors
          return reject('Filed could not be written.');
        }
        resolve(true);
      });
    });
  }

  /**
   * @inheritDoc
   */
  deleteFile(path: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      fs.unlink(path, (error) => {
        if (error) {
          console.error('File could not be deleted. \n File Path: ' + path + '\n Error: ' + error);
          // eslint-disable-next-line prefer-promise-reject-errors
          return reject('Filed could not be deleted.');
        }

        resolve(true);
      });
    });
  }

  /**
   * @inheritDoc
   */
  readFileSync(path: string): string {
    return fs.readFileSync(path);
  }

  /**
   * @inheritDoc
   */
  writeFileSync(path: string, content: string): undefined {
    return fs.writeFileSync(path, content);
  }

  /**
   * @inheritDoc
   */
  fileExistSync(path: string): boolean {
    return fs.existsSync(path);
  }

  /**
   * @inheritDoc
   */
  createFileSync(path: string): boolean {
    // eslint-disable-next-line no-throw-literal
    throw 'Not implemented';
  }

  /**
   * @inheritDoc
   */
  deleteFileSync(path: string): undefined {
    return fs.unlinkSync(path);
  }

  /**
   * @inheritDoc
   */
  readDirSync(path: string): any {
    return fs.readdirSync(path);
  }

  /**
   * @inheritDoc
   */
  readDir(path: string): Promise<any> {
    return new Promise<any>((resolve, reject) =>{
      fs.readdir(path, (error, dir) => {
        if (error) {
          console.error('Files could not be found. \n File Path: ' + path + '\n Error: ' + error);
          return resolve(false);
        }

        resolve(dir);
      });
    });
  }
}
