import {YoutubeProvider} from '../Providers/YoutubeProvider';
import {MusicProvider} from '../Providers/MusicProvider';
import {Command} from '../Core/Command';
import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {searchChannelByName} from '../Core/Utils/SearchChannelUtils';

/** Command to Play music on audio channel
 *  @class
 *  @extends Command
 */
export class PlayMusicCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const args = this.getArgs();
    const channelName = args.shift().replace('#', '');
    let searchString = args.join(' ');

    let channel = searchChannelByName(this.getMsg().guild.channels.cache, 'voice', channelName).first();

    let defaultChannel;
    const guild = await new FirebaseProvider().findGuild(this.getMsg().guild.id);
    if (guild['configs'] && guild['configs']['default-music']) {
      const defaultGuildName = guild['configs']['default-music'];
      defaultChannel = searchChannelByName(this.getMsg().guild.channels.cache, 'voice', defaultGuildName).first();
    }

    if (!channel && !defaultChannel) {
      this.reply(this.translation('error'));
      return;
    }

    if (!channel) {
      channel = defaultChannel;
      searchString = `${channelName} ${searchString}`;
    }

    const youtubeProvider = new YoutubeProvider();

    let link;
    let isLink = false;

    if (youtubeProvider.validateUrl(searchString)) {
      link = searchString;
      isLink = true;
    } else {
      link = await youtubeProvider.getFirstLink(searchString);
    }

    try {
      const musicProvider = new MusicProvider(channel);
      if (!isLink) {
        youtubeProvider.getMetadataFromUrl(link).then((metadata) => {
          this.reply(this.translation('message',
              {title: metadata['title'], link: link},
          ));
        });
      }
      const connection = await musicProvider.openChannel();
      const stream = youtubeProvider.getStreamFromUrl(link);
      musicProvider.startMusic(connection, stream);
    } catch (error) {
      console.log(`Something when wrong while starting music: ${error}\n${error.stack}`);
    }
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'play_music';
  }
}
