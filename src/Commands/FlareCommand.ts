import {MessageAttachment} from 'discord.js';
import {Command} from '../Core/Command';

/** Command to send a signal to guild members
 *  @class
 *  @extends Command
 */
export class FlareCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const attachment = new MessageAttachment('https://media.giphy.com/media/SXCr0czRagXorpiewG/giphy.gif', 'flare.gif');

    this.send(this.translation('response', {
      guild: this.getMsg().guild,
      member: this.getMsg().guild.roles.everyone,
    }), attachment);
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }
}
