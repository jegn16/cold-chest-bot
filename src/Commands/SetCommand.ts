import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

/** Command to Modify setting of bot
 *  @class
 *  @extends Command
 */
export class SetCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const guildId = this.getMsg().guild.id;
    const config = this.getArgs()[0];
    const value = this.getArgs()[1];

    const firebaseManager = new FirebaseProvider();
    firebaseManager.findGuild(guildId.toString()).then((guild) => {
      if (!guild) {
        guild = {};
      }
      if (!guild.configs) guild.configs = {};

      guild.configs[config] = value;

      return firebaseManager.updateGuild(guildId.toString(), guild);
    }).then(() => {
      this.reply(this.translation('message'));
    }).catch((error) => {
      console.log(`Error: ${error}`);
      this.reply(this.translation('error'));
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'IsAdmin',
      'WasPostedOnGuild',
    ];
  }
}
