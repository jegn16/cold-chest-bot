import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {CommandProvider} from '../Providers/ComandProvider';
import {Command} from '../Core/Command';

/** Command to register alias of commands
 *  @class
 *  @extends Command
 */
export class AliasCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const guildId = this.getMsg().guild.id;
    const args = this.getArgs();
    const command = args[0];
    const alias = args[1];
    const firebaseManager = new FirebaseProvider();
    const commandProvider = new CommandProvider();

    commandProvider.get(command).then((_) =>{
      firebaseManager.findGuild(guildId.toString()).then( (guild) => {
        if (!guild) {
          guild = {};
        }

        if (!guild.alias) guild.alias = {};

        guild.alias[alias] = command;

        return firebaseManager.updateGuild(guildId.toString(), guild);
      }).then(() => {
        this.reply(this.translation('created',
            {command: command, alias: alias}));
      }).catch((error) => {
        console.log('Error: ' + error);
        return this.reply(this.translation('error',
            {alias: alias}));
      });
    }).catch((errors) => {
      this.reply(this.translation('unknown_command',
          {command: command}));
    });
    this.reply(this.translation('preparing'));
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 2;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }
}
