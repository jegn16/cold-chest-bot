import Discord = require('discord.js');
import {Command} from '../Core/Command';

/** Command that provides wiki services
 * @class
 * @extends Command
 */
export class WikiCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const DDG = require('node-ddg-api').DDG;

    const ddg = new DDG('my-app-name');

    const search = this.getArgs().join(' ');

    ddg.instantAnswer( search, {}, (err, response) => {
      const embedResponse = new Discord.MessageEmbed();
      embedResponse.setColor('#4e0b7a');
      embedResponse.setTitle(`${response.Heading}`);
      if (response.AbstractURL !== '') {
        embedResponse.setURL(response.AbstractURL);
      }

      let description = this.translation('not_found');
      if (response.Abstract !== '') {
        description = response.Abstract;
      } else if (response.AbstractURL !== '') {
        description = this.translation('ambiguous');

        for (let i = 0; i < response.RelatedTopics.length; i++) {
          const related = response.RelatedTopics[i];
          if (related.FirstURL !== undefined) {
            let alternative = decodeURIComponent(related.FirstURL).replace('https://duckduckgo.com/', '');
            alternative = alternative.replace(/_/gi, ' ');
            description += `\n - ${alternative}`;
          }
        }
      }

      if (description.length > 2048) {
        description = description.slice(0, 2045);
        description += '...';
      }

      embedResponse.setDescription(description);

      if (response.Image !== '') {
        // embedResponse.setThumbnail(response.Image);
      }

      if (response.AbstractSource !== '') {
        embedResponse.setFooter(this.translation('footer', {
          source: response.AbstractSource,
        }));
      }

      this.send(embedResponse);

      console.log(response);
    });
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }
}
