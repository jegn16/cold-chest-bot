import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

/** Command to Remove alias of commands
 *  @class
 *  @extends Command
 */
export class RemoveAliasCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const guildId = this.getMsg().guild.id;
    const alias = this.getArgs()[0];
    const firebaseManager = new FirebaseProvider();

    firebaseManager.findGuild(guildId.toString()).then( (guild) => {
      if (!guild) {
        guild = {};
      }

      if (!guild.alias) guild.alias = {};

      if (!guild.alias[alias]) {
        // eslint-disable-next-line no-throw-literal
        throw 'Error';
      }

      guild.alias[alias] = null;

      return firebaseManager.updateGuild(guildId.toString(), guild);
    }).then(() => {
      this.reply(this.translation('removed',
          {alias: alias}));
    }).catch((error) => {
      console.log('Error: ' + error);
      return this.reply(this.translation('error',
          {alias: alias}));
    });

    this.reply(this.translation('preparing'));
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'remove_alias';
  }
}
