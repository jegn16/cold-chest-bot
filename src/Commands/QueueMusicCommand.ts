import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {YoutubeProvider} from '../Providers/YoutubeProvider';
import {Command} from '../Core/Command';
import {searchChannelByActiveUser, searchChannelByGuild} from '../Core/Utils/SearchChannelUtils';

/** Command to queue music to play
 *  @class
 *  @extends Command
 */
export class QueueMusicCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const search = this.getArgs().join(' ');

    const channels = searchChannelByGuild(
        this.getMsg().client.channels.cache,
        'voice',
        this.getMsg().guild,
    );
    const channel = searchChannelByActiveUser(channels, this.getMsg().client.user).first();

    if (!channel) {
      this.reply(this.translation('channel'));
      return;
    }

    const firebaseProvider = new FirebaseProvider();
    const youtubeProvider = new YoutubeProvider();
    let linkPromise;

    if (youtubeProvider.validateUrl(search)) {
      linkPromise = new Promise((success)=>{
        success(search);
      });
    } else {
      linkPromise = youtubeProvider.getFirstLink(search);
    }

    linkPromise.then((url) => {
      return firebaseProvider.findGuild(this.getMsg().guild.id.toString()).then((guild) => {
        if (!guild) guild = {};
        if (!guild.music_queue)guild.music_queue = {};

        const musicQueue = [];
        Object.keys(guild.music_queue).map(function(key, index) {
          musicQueue.push(guild.music_queue[key]);
        });
        guild.music_queue = musicQueue;

        guild.music_queue.push(url);

        return firebaseProvider.updateGuild(this.getMsg().guild.id.toString(), guild);
      });
    }).then((_) =>{
      this.reply(this.translation('queued'));
    }).catch((error) => {
      console.log(`Error: ${error}`);
      this.reply(this.translation('error'));
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'queue_music';
  }
}
