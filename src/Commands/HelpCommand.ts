import {FileManagerProvider} from '../Providers/FileManagerProvider';
import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

const config = require('../../config_files/config.json');
/** Command to show list of commands
 *  @class
 *  @extends Command
 */
export class HelpCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const prefix = config.prefix;
    const guildId = this.getMsg().guild.id;
    const fileManager = new FileManagerProvider();
    const commands = fileManager.readDirSync('./build/Commands').filter((file) => file.endsWith('.js'));
    const filebaseProvider = new FirebaseProvider();

    filebaseProvider.findGuild(guildId).then((guild) => {
      const aliasRelations = [];
      if (guild.alias) {
        for ( const alias in guild.alias) {
          if (Object.prototype.hasOwnProperty.call(guild.alias, alias)) {
            const originalCommand = guild.alias[alias];

            if (!aliasRelations[originalCommand]) aliasRelations[originalCommand] = [];
            aliasRelations[originalCommand].push(alias);
          }
        }
      }

      let explanation = this.translation('introduction');

      for (const commandFile in commands) {
        if (!Object.prototype.hasOwnProperty.call(commands, commandFile)) continue;
        const commandClassName = commands[commandFile].replace('.js', '');
        const CommandClass = require(`../Commands/${commands[commandFile]}`)[commandClassName];
        const command = new CommandClass(null, null) as Command;
        const commandName = commandClassName.replace('Command', '');

        const alias = (aliasRelations[commandName]) ? aliasRelations[commandName] : [];

        if (command.schema() == undefined) continue;

        if (command.validations().includes('IsAdmin')) explanation += `*`;
        explanation += `${prefix}${command.schema()} - ${command.description()}`;

        if (alias.length > 0) {
          explanation += this.translation('alias', {
            aliases: alias.join(','),
          });
        } else {
          explanation += `\n`;
        }
      }

      explanation += this.translation('conclusion');

      console.log(`Total Characters: ${explanation.length}`);
      let content = explanation;
      if (content.length > 2000) {
        do {
          let page = content.substring(0, 2000);
          const remainsPosition = page.lastIndexOf('\n');
          content = page.substring(remainsPosition+1) + content.substring(2000);
          page = page.substring(0, remainsPosition);

          this.send(page);
        } while (content.length > 2000);
      }

      this.send(content);
    }).catch((error) => {
      console.log(`Error: ${error}`);
      this.reply(this.translation('error'));
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'ShouldListen',
    ];
  }
}
