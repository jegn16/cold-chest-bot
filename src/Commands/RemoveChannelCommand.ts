import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

/** Command to Remove channel from bot
 *  @class
 *  @extends Command
 */
export class RemoveChannelCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const guildId = this.getMsg().guild.id;
    const removedChannel = this.getMsg().mentions.channels.first();
    const firebaseManager = new FirebaseProvider();

    firebaseManager.findGuild(guildId.toString()).then( (guild) => {
      if (!guild) guild = {};

      if (!guild.channels) guild.channels = {};

      const identicalChannels = guild.channels[removedChannel.id];

      if (!identicalChannels) {
        // eslint-disable-next-line no-throw-literal
        throw 'Error';
      }

      guild.channels[removedChannel.id] = null;

      return firebaseManager.updateGuild(guildId.toString(), guild);
    }).then(() => {
      this.reply(this.translation('removed'));
    }).catch((error) => {
      console.log(`Error: ${error}`);
      return this.reply(this.translation('error',
          {channel: removedChannel}));
    });
    this.reply(this.translation('preparing'));
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'IsAdmin',
      'WasPostedOnGuild',
      'AffectedChannelExists',
    ];
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'remove_channel';
  }
}
