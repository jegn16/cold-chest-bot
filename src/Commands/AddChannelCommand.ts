import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

/** Command to manage Adding a Channel to Bot Control
 *  @class
 *  @extends Command
 */
export class AddChannelCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const guildId = this.getMsg().guild.id;
    const addChannel = this.getMsg().mentions.channels.first();

    const firebaseManager = new FirebaseProvider();
    firebaseManager.findGuild(guildId.toString()).then((guild) => {
      if (!guild) {
        guild = {};
      }
      if (!guild.channels) guild.channels = {};
      const identicalChannels = guild.channels[addChannel.id];

      if (identicalChannels) {
        // eslint-disable-next-line no-throw-literal
        throw `Error`;
      }

      guild.channels[addChannel.id] = true;

      return firebaseManager.updateGuild(guildId.toString(), guild);
    }).then(() => {
      this.reply(this.translation('added'));
    }).catch((error) => {
      console.log(`Error: ${error}`);
      return this.reply(this.translation('error',
          {channel: addChannel}));
    });
    this.reply(this.translation('preparing'));
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'IsAdmin',
      'WasPostedOnGuild',
      'AffectedChannelExists',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'add_channel';
  }
}
