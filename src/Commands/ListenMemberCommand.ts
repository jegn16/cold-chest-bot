import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

/** Command to listen a member
 *  @class
 *  @extends Command
 */
export class ListenMemberCommand extends Command {
  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'listen_member';
  }

  /**
   * @inheritDoc
   */
  async execute() {
    const guildOwner = this.getMsg().guild.owner.user;
    const guildId = this.getMsg().guild.id;

    if (this.getMsg().guild.member(this.getMsg().client)) {
      this.reply(this.translation('not_member'));
      return;
    }

    console.log(this.getMsg().mentions.users);
    const user = this.getMsg().mentions.users.first();

    console.log('User ' + user );

    if (guildOwner.equals(user)) {
      this.reply(this.translation('admin'));
      return;
    }

    const member = this.getMsg().guild.member(user);

    const firebaseManager = new FirebaseProvider();

    firebaseManager.findGuild(guildId.toString()).then((guild) => {
      if (!guild) {
        guild = {};
      }

      if (!guild.ignored_members) {
        guild.ignored_members = {};
      }

      const ignoredCoincidences = guild.ignored_members[member.id.toString()];

      if (!ignoredCoincidences) {
        // eslint-disable-next-line no-throw-literal
        throw 'Error';
      }

      guild.ignored_members[member.id.toString()] = null;

      return firebaseManager.updateGuild(guildId.toString(), guild);
    }).then((_) => {
      this.reply(this.translation('listen', {
        member: member,
      }));
    }).catch((error) => {
      console.log(`Error: ${error}`);
      this.reply(this.translation('error', {
        member: member,
      }));
    });
  }

  /**
   * @inheritDoc
   */
  validations(): string[] {
    return [
      'NumberArgs',
      'IsAdmin',
      'WasPostedOnGuild',
      'AffectedExists',
      'AffectedIsMember',
    ];
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }
}
