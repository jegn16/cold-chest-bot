import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';
import {searchChannelByActiveUser, searchChannelByGuild} from '../Core/Utils/SearchChannelUtils';
import {VoiceChannel} from 'discord.js';

/** Command to stop music
 *  @class
 *  @extends Command
 */
export class StopMusicCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const channels = searchChannelByGuild(
        this.getMsg().client.channels.cache,
        'voice',
        this.getMsg().guild,
    );
    const channel = searchChannelByActiveUser(channels, this.getMsg().client.user).first() as VoiceChannel;

    if (!channel) {
      this.reply(this.translation('error'));
      return;
    }

    const firebaseProvider = new FirebaseProvider();
    firebaseProvider.findGuild(this.getMsg().guild.id.toString()).then((guild) =>{
      if (guild == null || guild.music_queue == null) return;
      guild.music_queue = null;
      return firebaseProvider.updateGuild(this.getMsg().guild.id.toString(), guild);
    }).then((_) => {
      channel.leave();
    }).catch((error) => {
      console.log(`Error while stopping music ${error}`);
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'stop_music';
  }
}
