import {MessageAttachment} from 'discord.js';
import {Command} from '../Core/Command';

/** Command that provides a special blocker
 * @class
 * @extends Command
 */
export class WeebBlockCommand extends Command {
  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'weeb_block';
  }

  /**
   * @inheritDoc
   */
  async execute() {
    let times = Math.abs(this.getArgs()[0]);

    if (!times) {
      times = 1;
    }

    if (times > 6) {
      times = 6;
    }

    for (let i = 0; i < times; i++) {
      // eslint-disable-next-line max-len
      const url = 'https://cdn.discordapp.com/attachments/509230664391065602/730213889798832178/SPOILER_content-blocked-red-rubber-stamp-over-a-white-background-G3PB1R-1.png';
      const attachment = new MessageAttachment(url);

      this.send(this.translation('response'), attachment);
    }
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }
}
