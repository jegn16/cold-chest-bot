import {Command} from '../Core/Command';

/** Command that provides a special blocker
 * @class
 * @extends Command
 */
export class NoPuedoCommand extends Command {
  /**
     * @inheritDoc
     */
  async execute() {
    const actions = [
      'No puede ese día', 'Está ocupado', 'Quiere otra fecha', 'Irá en otra ocasión', 'Si quiere pero no puede',
      'No va a ir', 'Quiere cambiar la fecha', 'Ya no fue', 'Esta maraqueando', 'Iría pero no puede',
      'Si va ir, a menos que', 'Ya no va a ir', 'Estaba saliendo pero ya no', 'Se apendejo y no va',
      'Se le fue la hora', 'No se dio cuenta del tiempo', 'Le da ansiedad y no va', 'Le dio patatus',
      'Deja de mamar y vamos', 'Hara cosplay de Berny',
    ];

    const reasons = [
      'Tiene mucho trabajo', 'Le duele el estomago', 'Se siente mal', 'Tiene otro evento', 'No durmió bien',
      'Es maraca', 'Es 100% maraca, etapa 4, terminal', 'No tiene transporte', 'Va a dormir', 'Ya murió',
      'Se durmió', 'Está muy cansado', 'Le duele la cabeza', 'Bernardo', 'He is going full berny',
      'Se quedó jugando Mount and Blade', 'Se quedó jugando Spelunky', 'Se quedó jugando For Honor',
      'Se quedo jugando JRPGs', 'Se quedo jugando League of Legends material de donde se basa Arcane ' +
      'la popular serie de Netflix creada por la pequeña compañía Riot Games', 'Se quedó jugando Monster hunter',
      'Se quedó jugando juegos de pelea', 'Se quedó jugando DBD', 'Ya es tarde a esa hora', 'Su mami no le deja',
      'Ya cantó la pandilla telmex', 'Le pegan', 'Esta en la tonta Tejas', 'Se quedó jugando Gears',
      'Se quedó jugando Halo', 'Le da miedo la calle', 'Ya le salieron raices', 'le caen mal',
      'No encontró sus llaves', 'No tiene lana (aunque le inviten)', 'Pensó que era otro dia',
      'Ya se le hizo tarde', 'Le duele el pipi', 'Está ON Amourath', 'Vio que era malo en reddit',
      'El nunca existió, era tu imaginación', 'Fue isekaided', 'Fue transmigrado a otro mundo',
      'Se quedó leyendo manga', 'Le dio diarrea', '? Nadie sabe',
      'No puede ser que no quieran salir con los panas deos meo', 'Le duele el ano',
    ];

    let user;
    if (this.getMsg().mentions.users.size > 0) {
      user = this.getMsg().mentions.users.first().username;
    } else {
      user = this.getMsg().author.username;
    }

    try {
      this.send(this.translation('response',
          {
            user,
            action: actions[this.getRandomNumber(actions.length)],
            reason: reasons[this.getRandomNumber(reasons.length)],
          },
      ));
    } catch (error) {
      console.log(`Something went wrong while being maraca: ${error}\n${error.stack}`);
    }
  }

  /**
     * @inheritDoc
     */
  getNumberArguments(): number {
    return 0;
  }

  /**
     * @inheritDoc
     */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
     * @inheritDoc
     */
  getRandomNumber(max: number): number {
    return Math.floor(Math.random()*max);
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'no_puedo';
  }
}
