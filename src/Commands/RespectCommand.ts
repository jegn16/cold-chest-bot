import {Command} from '../Core/Command';

/** Command to show respect to does that are now inactive
 *  @class
 *  @extends Command
 */
export class RespectCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const inactiveMembers = [];

    this.getMsg().guild.members.fetch().then((members) => {
      console.log(`Total members: ${members}`);
      members.each((memberId) => {
        const member = this.getMsg().guild.member(memberId);
        if (member.user.presence.status == 'offline') {
          inactiveMembers.push(member);
        }
      });

      if (inactiveMembers.length > 0) {
        this.reply(this.translation('response', {
          members: inactiveMembers,
        }));
      } else {
        this.reply(this.translation('error'));
      }
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }
}
