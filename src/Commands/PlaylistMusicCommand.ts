import {YoutubeProvider} from '../Providers/YoutubeProvider';
import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';
import {searchChannelByActiveUser, searchChannelByGuild} from '../Core/Utils/SearchChannelUtils';

/** Command to list al music in queue
 *  @class
 *  @extends Command
 */
export class PlaylistMusicCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const channels = searchChannelByGuild(
        this.getMsg().client.channels.cache,
        'voice',
        this.getMsg().guild,
    );
    const channel = searchChannelByActiveUser(channels, this.getMsg().client.user).first();

    if (!channel) {
      this.reply(this.translation('channel'));
      return;
    }

    const firebaseProvider = new FirebaseProvider();
    const youtubeProvider = new YoutubeProvider();
    firebaseProvider.findGuild(this.getMsg().guild.id.toString()).then(async (guild) => {
      if (!guild) guild = {};
      if (!guild.music_queue)guild.music_queue = {};

      const musicQueue = [];
      Object.keys(guild.music_queue).map(function(key, index) {
        musicQueue.push(guild.music_queue[key]);
      });
      guild.music_queue = musicQueue;

      let list = this.translation('introduction');
      const top = (guild.music_queue.length > 10) ? 10: guild.music_queue.length;
      for (let i = 0; i < top; i++) {
        const link = guild.music_queue[i];
        const metadata = await youtubeProvider.getMetadataFromUrl(link);

        list += this.translation('song', {
          name: metadata['title'],
          link: link,
        });
      }
      if (guild.music_queue.length > 10) {
        list += this.translation('conclusion', {
          total: guild.music_queue.length,
        });
      }
      this.reply(list);
    }).catch((error) => {
      console.log(`Error: ${error}`);
      this.reply(this.translation('error'));
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'playlist';
  }
}
