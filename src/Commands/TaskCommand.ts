import {Command} from '../Core/Command';
import {FirebaseProvider} from '../Providers/FirebaseProvider';

/** Command to set task
 *  @class
 *  @extends Command
 */
export class TaskCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const args = this.getArgs();
    const event = args[0];
    const action = args[1];
    const trigger = args[2];
    const target = (args[3]) ? args[3].replace('<#', '').replace('>', '') : null;
    const guildId = this.getMsg().guild.id;

    const firebaseManager = new FirebaseProvider();

    let guild = await firebaseManager.findGuild(guildId);

    if (!guild) guild = {};
    if (!guild.tasks)guild.tasks = {};
    if (!guild.tasks[event])guild.tasks[event] = {};
    let tasksOfEvent = guild.tasks[event];

    const tasks = [];
    Object.keys(tasksOfEvent).map(function(key, index) {
      tasks.push(tasksOfEvent[key]);
    });

    const triggerParts = trigger.split('|');
    if (!triggerParts[1]) triggerParts[1] = 1;
    tasksOfEvent = tasks;

    tasksOfEvent.push({
      action: action,
      expression: triggerParts[0],
      total: triggerParts[1],
      target: target,
    });

    guild.tasks[event] = tasksOfEvent;

    await firebaseManager.updateGuild(guildId, guild);

    this.reply(this.translation('success'));
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 3;
  }
}
