import {Command} from '../Core/Command';

/** Command to change nickname of users
 *  @class
 *  @extends Command
 */
export class ChangeNicknameCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const newNickname = this.getArgs()[1];
    console.log('Nickname: ' + newNickname);

    if (this.getMsg().guild.member(this.getMsg().client)) {
      this.reply(this.translation('not_member'));
      return;
    }

    console.log(this.getMsg().mentions.users);
    const user = this.getMsg().mentions.users.first();

    console.log('User ' + user );

    const member = this.getMsg().guild.member(user);

    // const oldNickname = member.nickname;

    member.setNickname(newNickname, 'You  earned it buddy').then((updatedMember) => {
      this.reply(this.translation('changed', {
        nickname: newNickname,
        member: member,
        user: user.username,
      }));
    }).catch((error) =>{
      console.log(`Error: ${error}`);
      this.reply(this.translation('error'));
    });

    this.reply(this.translation('preparing'));
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 2;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'WasPostedOnGuild',
      'ShouldListen',
      'AffectedExists',
      'AffectedIsMember',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'change_nickname';
  }
}
