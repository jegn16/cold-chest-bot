import {CommandProvider} from '../Providers/ComandProvider';
import {FirebaseProvider} from '../Providers/FirebaseProvider';
import {Command} from '../Core/Command';

const config = require('../../config_files/config.json');
/** Command to manage gifs
 *  @class
 *  @extends Command
 */
export class GifCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const args = this.getArgs();
    const command = args.shift();
    const name =args.shift();
    const urls = args;
    const guildId = this.getMsg().guild.id;

    const firebaseManager = new FirebaseProvider();
    const commandProvider = new CommandProvider();

    let successMessage = '';
    let errorMessage = this.translation('error.unknown');

    commandProvider.get(name).then((_) => {
      successMessage = this.translation('error.command', {
        name: name,
      });
      this.reply(successMessage);
    }).catch((_) => {
      firebaseManager.findGuild(guildId.toString()).then((guild) => {
        if (guild.alias != undefined && guild.alias[name] != null) {
          errorMessage = this.translation('missing', {
            name: name,
            command: command,
          });
          // eslint-disable-next-line no-throw-literal
          throw 'Alias in use';
        }

        if (urls.length == 0 &&
            command.toLowerCase() != 'remove' &&
            command.toLowerCase() != 'show' &&
            command.toLowerCase() != 'list') {
          errorMessage = this.translation('error.action');
          // eslint-disable-next-line no-throw-literal
          throw 'No urls';
        }

        if (guild.gifs == undefined) {
          guild.gifs = {};
        }

        if (command.toLowerCase() == 'create') {
          let concatURls = ' ' + urls.join(' ').replace(/\\\\\$/gi, '$') + ' ';
          let concats : any[] = concatURls.match(/ ".[^"]*"/gi);
          if (!concats) {
            concats = [];
          }

          for (let i = 0; i < concats.length; i++) {
            concatURls = concatURls.replace(concats[i], ' ');
            concats[i] = concats[i].replace(/"/gi, '');
          }
          let finalURls = (concatURls.length == 0) ? [] : concatURls.trim().split(' ');
          finalURls = finalURls.filter((value) => {
            return value != '';
          });
          finalURls = concats.concat(finalURls);

          guild.gifs[name] = finalURls;
          successMessage = this.translation('created', {
            name: name,
          });
        }

        if (command.toLowerCase() == 'add') {
          if (!guild.gifs[name]) {
            errorMessage = this.translation('error.missing', {
              name: name,
            });
            // eslint-disable-next-line no-throw-literal
            throw `there are no gifs with name ${name}`;
          }

          const gifs = [];
          Object.keys(guild.gifs[name]).map(function(key, index) {
            gifs.push(guild.gifs[name][key]);
          });
          guild.gifs[name] = gifs;

          guild.gifs[name] = guild.gifs[name].concat(urls);
          successMessage = this.translation('added', {
            name: name,
          });
        }

        if (command.toLowerCase() == 'remove') {
          guild.gifs[name] = null;
          successMessage = this.translation('removed', {
            name: name,
          });
        }

        if (command.toLowerCase() == 'show') {
          const links = [];
          Object.keys(guild.gifs[name]).map(function(key, index) {
            links.push(guild.gifs[name][key]);
          });

          const numLinks = links.length;
          const selection = Math.round(Math.random() * ( numLinks - 1));
          this.send(links[selection]);
          return true;
        }

        if (command.toLowerCase() == 'list') {
          const prefix = config.prefix;
          let response = this.translation('list.preface');
          Object.keys(guild.gifs).map((key, index) => {
            const length = Object.keys(guild.gifs[key]).length;
            response += this.translation('list.item', {
              prefix: prefix,
              command: key,
              total: length,
            });
          });

          console.log(`Characters: ${response.length}`);
          this.send(response, {split: true});
        }

        return firebaseManager.updateGuild(guildId.toString(), guild);
      }).then((_) => {
        if (successMessage != '') this.reply(successMessage);
      }).catch((error) => {
        console.log(`Error: ${error}`);
        this.reply(errorMessage);
      });
    });
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 1;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'NumberArgs',
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }
}
