import {MusicProvider} from '../Providers/MusicProvider';
import {Command} from '../Core/Command';
import {searchChannelByActiveUser, searchChannelByGuild} from '../Core/Utils/SearchChannelUtils';

/** Command to play next song on queue
 *  @class
 *  @extends Command
 */
export class NextMusicCommand extends Command {
  /**
   * @inheritDoc
   */
  async execute() {
    const channels = searchChannelByGuild(
        this.getMsg().client.channels.cache,
        'voice',
        this.getMsg().guild,
    );
    const channel = searchChannelByActiveUser(channels, this.getMsg().client.user).first();

    if (!channel) {
      this.reply(this.translation('error'));
      return;
    }

    new MusicProvider(channel).loadNextSong();
    this.reply(this.translation('response'));
  }

  /**
   * @inheritDoc
   */
  getNumberArguments(): number {
    return 0;
  }

  /**
   * @inheritDoc
   */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
   * @inheritDoc
   */
  protected getCommandName(): string {
    return 'next_music';
  }
}
