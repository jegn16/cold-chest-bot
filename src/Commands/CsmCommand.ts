import {Command} from '../Core/Command';

/** Command that provides a special blocker
 * @class
 * @extends Command
 */
export class CsmCommand extends Command {
  /**
     * @inheritDoc
     */
  async execute() {
    const names = ['Basto', 'Lalo', 'Gabo', 'Jony', 'Gil', 'Felix', 'Julián', 'Chay', 'Román', 'Tony',
      'El hermano de Gil', 'Grupo Macro', 'Kwan', 'Vlad', 'MidSoft',
      'Pablo de Grupo Macro', 'Aguiloco', 'El pendejo que me invocó', 'Tu hermana',
      'Op', 'Maestro Limpio', 'Cubicode', 'Victor', 'Vecino', 'Kwan', 'KSquare', 'Yellowme',
      'Boyfriend', 'El boyfriend de boyfriend', '3 tortas', 'Vlad', 'RL',
      'Mi ano', 'Woody', 'Jessie', 'AMLO', 'Ontiveros', 'Enfermera',
      'Goriloco', 'Padrino', 'El Bucky', 'Max, el perrito de Grupo Macro', 'Peludito', 'Pecho frío',
      'Gastón', 'Evaristo', 'Romario', 'Esteban', 'Burguitos', 'La novia de Chay (de Construcción)',
      'Esteban', 'Quiroz', 'Camaleón', 'Bernardo', 'Berny', 'Irma', 'La parca'];

    const actions = ['se casó con', 'se besó con', 'le dio los mamuts a', 'funó a', 'se abrazó con',
      'le dio su lechita a', 'mandó a chsm a', 'mató a', 'se enamoró de', 'cuckeó a', 'le soltó un vergazo a',
      'le escondió la mochila a', 'mató a sentones a', 'le donó 20 pesos a', 'le dio un beso a', 'se enojó con',
      'miró con deseo a'];

    const reasons = ['lo ama', 'perdió en Gears', 'perdió en Fighterz con un Broly',
      'reprobó Construcción, F', 'está harto de la vida', 'lo agarró viendo un stream de Amouranth', 'es virgen',
      'es joto', 'encontró su Onlyfans', 'se quiere morir alv', 'huele feo', 'no sabe programar',
      'no sabe hacer git rebase', 'programa con las patas', 'la tiene grande', 'la tiene chiquita',
      'son compas', 'está enamorado', 'le mató a su chanchito', 'se siente solo', 'le mandó un meme de la tía',
      'no se quieren juntar a jugar los panas', 'es putaku', 'se chingó la rodilla', 'perdió una apuesta',
      'no aprobó Calidad', 'encontró su carpeta secreta', 'hay que hacer más Wordpress',
      'es su waifu', 'se atragantó con su reata', 'csm el América', 'está bien rikolino',
      'lo agarró jugando Aristoputas', 'es racista', 'es negro', 'es tóxico', 'está feedeando'];

    try {
      this.send(this.translation('response',
          {
            name: names[this.getRandomNumber(names.length)], action: actions[this.getRandomNumber(actions.length)],
            reason: reasons[this.getRandomNumber(reasons.length)], subject: names[this.getRandomNumber(names.length)],
          },
      ));
    } catch (error) {
      console.log(`Something went wrong while mentoring someone's mother: ${error}\n${error.stack}`);
    }
  }

  /**
     * @inheritDoc
     */
  getNumberArguments(): number {
    return 0;
  }

  /**
     * @inheritDoc
     */
  validations(): Array<string> {
    return [
      'WasPostedOnGuild',
      'ShouldListen',
    ];
  }

  /**
     * @inheritDoc
     */
  getRandomNumber(max: number): number {
    return Math.floor(Math.random()*max);
  }
}
