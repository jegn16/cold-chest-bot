import {TranslationProvider} from '../Providers/TranslationProvider';
import {Validator} from './Validator';

/** Class that represents the command an user can call through a key word
 *  @class
 *  @abstract
 */
export abstract class Command {
    private readonly msg;
    private readonly args;

    /** Retrieves the list of preconditions that every message needs to fulfill before the command
     * can be executed.
     * @return {Array<string>} list of preconditions
     */
    abstract validations(): Array<string>;
    /** Retrieves the minimum number of args that the command requires before it can be executed
     *  @return {number}
     */
    abstract getNumberArguments(): number;
    /** Executes the service provided by command
     * @async
     * @implements execute
     */
    abstract execute();

    /** Base Constructor
     * @param {Message} msg Message that requested execution of command
     * @param {Object} args Arguments provided to command by user for it's execution
     */
    public constructor(msg, args) {
      this.msg = msg;
      this.args = args;
    }

    /** Retrieves internationalize description of command
     * @return {string} Internationalize description of command
     */
    public description(): string {
      return this.translation('description');
    }

    /** Retrieves internationalize scheme of command
     * @return {string} Internationalize scheme of command
     */
    public schema(): string {
      return this.translation('schema');
    }

    /** Validates Message to see if requirements to trigger the command have been meet
     * @async
     * @return {Promise<boolean>} Promise with a boolean indicating if requirements have been meet
     */
    public async validate(): Promise<boolean> {
      let isValid = true;
      const validations = this.validations();
      for (const validation of validations) {
        const validationName = validation;
        const ValidatorClass = require(`../Validators/${validationName}Validator.js`)[`${validationName}Validator`];
        const validator = new ValidatorClass(this) as Validator;
        const validationResult = await validator.validate(this.msg, this.args);
        if (!validationResult) {
          isValid = false;
          this.reply(validator.errorMessage());
          break;
        }
      }
      return isValid;
    }

    /** Retrieves name of current command
     * @protected
     * @return {string} Name of Command
     */
    protected getCommandName(): string {
      return this.constructor.name.toLocaleLowerCase().replace('command', '');
    }

    /** Retrieves message that triggered the command
     * @protected
     * @return {Message} Message
     */
    protected getMsg() {
      return this.msg;
    }

    /** Retrieves arguments provided to the command when triggered
     * @protected
     * @return {Object} Arguments provided
     */
    protected getArgs() {
      return this.args;
    }

    /** Send a reply to user that trigger the command through the channel where is as triggered
     * @param {MessageEmbed|String} message Message to be send
     * @param {MessageAttachment} attachment (Optional) Attachment to include with the message.
     * @protected
     */
    protected reply(message: any, attachment = undefined) {
      this.msg.reply(message, attachment);
    }

    /** Sends a message to the channel where command was triggered
     * @param {MessageEmbed|String} message Message to be send
     * @param {MessageAttachment} attachment (Optional) Attachment to include with the message.
     * @protected
     */
    protected send(message: any, attachment = undefined) {
      this.msg.channel.send(message, attachment);
    }

    /** Method to recover internationalize text of command
     * @param {string} identifier id of string to retrieve
     * @param {Object} args Pairs variable:value to be replace in string
     * @param {boolean} fullRoute Indicate where identifier is a full id from the internationalization file root
     *  or a relative id from the subset that belongs to this commands. Default: false.
     * @protected
     * @return {string} internationalized string corresponding to identifier
     */
    protected translation(identifier: string, args= {}, fullRoute = false): string {
      let route;
      if (fullRoute) {
        route = identifier;
      } else {
        route = 'command.'+ this.getCommandName() + '.' + identifier;
      }
      return TranslationProvider.getTranslation(route, args);
    }
}
