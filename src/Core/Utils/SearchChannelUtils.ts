import {Collection, Guild, GuildChannel, Snowflake, User} from 'discord.js';

/** Search a channel on a guild by name and type
 *
 * @param {Collection<Snowflake, GuildChannel>} channels channels to search on
 * @param {string} type Type of channel. Possible values: voice, text, category, news, store o unknown
 * @param {string} channelName Name of the channel or Identifier of channel
 * @return {Collection<Snowflake, GuildChannel>} Collections of channels that meet criteria
 */
export function searchChannelByName(
    channels: Collection<Snowflake, GuildChannel>,
    type: string,
    channelName: string,
): Collection<Snowflake, GuildChannel> {
  return channels.filter(
      (channel) => {
        if (channel.type != type) return false;
        if (channelName.startsWith('<') && channelName.endsWith('>')) {
          return channel.id == channelName.replace('<', '').replace('>', '');
        } else {
          return channel.name.replace(' ', '-').toLowerCase() == channelName.toLowerCase();
        }
      },
  );
}

/** Search a channels by the guild it belongs
 *
 * @param {Collection<Snowflake, GuildChannel>} channels Channels to search through
 * @param {string} type Type of channel. Possible values: voice, text, category, news, store o unknown
 * @param {Guild} guild Guild to search by
 * @return {Collection<Snowflake, GuildChannel>} Collections of channels that meet criteria
 */
export function searchChannelByGuild(
    channels: Collection<Snowflake, GuildChannel>,
    type: string,
    guild: Guild,
): Collection<Snowflake, GuildChannel> {
  return channels.filter( (channel) => {
    const isVoiceChannel = channel.type == type;
    const belongsToGuild = channel.guild.equals(guild);
    return isVoiceChannel && belongsToGuild;
  });
}

/** Search channels by an active user
 *
 * @param {Collection<Snowflake, GuildChannel>} channels Channels to search through
 * @param {User} user User to search by
 * @return {Collection<Snowflake, GuildChannel>} Collections of channels that meet criteria
 */
export function searchChannelByActiveUser(
    channels: Collection<Snowflake, GuildChannel>,
    user: User,
): Collection<Snowflake, GuildChannel> {
  return channels.filter( (channel) => {
    return channel.members.filter((member) => member.user.id == user.id).size == 1;
  });
}

