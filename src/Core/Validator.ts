import {ValidatorInterface} from './ValidatorInterface';
import {Command} from './Command';

/** Class that represents a validation that is required before a command is executed
 * @class
 * @abstract
 * @implements {ValidatorInterface}
 */
export abstract class Validator implements ValidatorInterface {
    private command;

    /** Base constructor
     * @param {Command} command Command that requires validations
     */
    constructor(command: Command) {
      this.command = command;
    };

    /** Get command being validated
     * @protected
     * @return {Command} Command being validated
     */
    protected getCommand(): Command {
      return this.command;
    }

    /** Retrieves messages to show if validation fails
     * @return {string} Message to show
     */
    abstract errorMessage(): string;

    /** Execute Validation
     * @param {Message} msg Message that is trying to execute command
     * @param {Object} args Arguments provided to command
     */
    abstract validate(msg, args): Promise<boolean>;
}

