export interface ValidatorInterface {

    validate(msg, args): Promise<boolean>;

    errorMessage(): string
}
