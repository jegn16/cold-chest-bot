## [Unreleased]
### Fixed
- Bug where list of gifs are not displayed if it is longer that max character per message
- Bug where language is ignored when a tasks conditions are meet, and the tasks is carry out

## [v0.7.2-Beta] - 2020-12-3
### Added
- Support to declare default music channel

## [v0.6.2-Beta] - 2020-12-1
### Fixed
- Bug preventing Gif command to register new gifs if content is not surrounded by ""

## [v0.6.1-Beta] - 2020-10-23
### Added 
- Support to concatenate commands with a configurable command separator
- Support to store new concatenated commands trough Gif command
- Support to declare monitoring conditions with command !task
- Support to detect reactions to a message
- Copy, reference, make gif are know available as possible tasks to carry out

## [v0.6] - 2020-09-28 
### Added
- Added new Git command option to list all gif registered for each guild

### Changed
- Established a more standardized command class to have a more consistent behaviour across al command.
- Now PlayMusic command shows information of pick video when ask to play music throw a search query.
- Now PlayMusic and QueueMusic will pass through youtube link directly rather than make a search with that link as it did previously.
- Restructure command to streamline integration with validations

### Fixed
- Help command will split in 2 or more replies if text excess the 2000 characters cap.

## [v0.5] - 2020-08-18
### Added
- Support to internationalize all commands responses through a YML file per language.
- New Commands Gif, NextMusic, PlaylistMusic, QueueMusic y Wiki

## [v0.4] - 2020-07-10
### Added
- Firebase integration to store customization information.
- New  Commands PlayMusic y Help.
- 

### Changed
- Local file system integration is being replaced by firebase integration.
- Updated command to use firebase integration.
- PlayMusic now support channels with space in their name by replacing the space with -.

## [v0.3] - 2020-07-09
### Added
- Support for Validation prior to execute command.

## [v0.2] - 2020-07-09
### Added
- Support for initialize guild customization file on bot registration.

## [v0.1] - 2020-07-07
- Initialization of project

