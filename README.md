Cold Chest Bod

Bot aiming to offer an easy to expand repertory of commands

Along the objectives of these projects are:
1. Offer a configurable bot with a grant access first approach in interaction across all channels of a guild
2. Rol base accessibility of commands
3. An expandable repertory of specialize commands
4. Internationalization support in a per channel bases

Development process

- Make changes on /src folder as needed them compile files with command <code>tsc</code>

Run project

- Once project has been compiled run it with command <code>node build/main.js</code>

Requirements

- Node 12 or newer
- npm 6 or newer

